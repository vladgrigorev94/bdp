const selectedPackageInit = {
  name: '',
  scope: '',
  version: '',
  description: '',
  keywords: [],
  date: '',
  links: {
    npm: '',
    homepage: '',
    repository: '',
    bugs: ''
  },
  author: {
    name: ''
  },
  publisher: {
    username: '',
    email: ''
  },
  maintainers: [
    {
      username: '',
      email: ''
    }
  ]
}

export default () => {
  return selectedPackageInit
}
